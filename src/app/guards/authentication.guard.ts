import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlSegment,
} from '@angular/router';

import { UserRole } from '../models/user-roles.enum';
import { SessionService } from '../services/util/session.service';
import { UserManagementService } from '../services/util/user-management.service';
import { EventService } from 'app/services/util/event.service';
import { filter, take } from 'rxjs';

@Injectable()
export class AuthenticationGuard implements CanActivate {
  constructor(
    private userManagementService: UserManagementService,
    private router: Router,
    private sessionService: SessionService,
    private eventService: EventService,
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean {
    const url = decodeURI(state.url);
    if (route.data.superAdmin) {
      const wasAdmin = this.isSuperAdmin();
      this.userManagementService
        .getUser()
        .pipe(
          filter((v) => Boolean(v)),
          take(1),
        )
        .subscribe((user) => {
          if (user.isSuperAdmin === wasAdmin) {
            return;
          }
          if (user.isSuperAdmin) {
            this.router.navigate([url]);
          } else {
            this.onNotAllowed();
          }
        });
      return wasAdmin;
    }
    const requiredRoles = (route.data['roles'] ?? []) as UserRole[];
    let wasAllowed = null;
    wasAllowed = this.sessionService.validateNewRoute(
      route.params.shortId,
      this.parseRole(url),
      requiredRoles,
      (allowed, redirect) => {
        if (redirect !== undefined) {
          this.redirect(redirect, route.url);
          return;
        }
        if (wasAllowed === null) {
          return;
        }
        if (allowed === wasAllowed) {
          return;
        }
        if (!allowed) {
          this.onNotAllowed();
          return;
        }
        if (wasAllowed !== null) {
          this.router.navigate([url]);
        }
      },
    );
    if (!wasAllowed) {
      this.onNotAllowed();
    }
    return wasAllowed;
  }

  private redirect(role: UserRole, segments: UrlSegment[]) {
    let url = '/participant/';
    if (role === UserRole.CREATOR) {
      url = '/creator/';
    } else if (role === UserRole.EXECUTIVE_MODERATOR) {
      url = '/moderator/';
    }
    url += segments.map((segment) => segment.path).join('/');
    this.router.navigate([url]);
  }

  private parseRole(url: string): UserRole {
    if (url.startsWith('/creator')) {
      return UserRole.CREATOR;
    } else if (url.startsWith('/moderator')) {
      return UserRole.EXECUTIVE_MODERATOR;
    } else if (url.startsWith('/participant')) {
      return UserRole.PARTICIPANT;
    }
    return null;
  }

  private isSuperAdmin() {
    return this.userManagementService.getCurrentUser()?.isSuperAdmin;
  }

  private onNotAllowed() {
    this.router.navigate(['/']).then(() => {
      setTimeout(() => this.eventService.broadcast('not-authorized'));
    });
  }
}
